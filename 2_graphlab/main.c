#include <stdlib.h>
#include <stdio.h>
#include "graph.h"

int main (int argc, char *argv[]) {
  vertex_t *vhead, *vp, *temp_v;
  int i;
  adj_vertex_t *adj_v, *temp_adjv;

  vhead = NULL;
  i=1;

  while (i < argc) {
    add_edge(&vhead, argv[i], argv[i+1], atoi(argv[i+2]));
    i = i+3;
  }

  printf("Adjacency list:\n");
  for (vp = vhead; vp != NULL; vp = vp->next) {
    printf("  %s: ", vp->name);
    for (adj_v = vp->adj_list; adj_v != NULL; adj_v = adj_v->next) {
      printf("%s(%d) ", adj_v->vertex->name, adj_v->edge_weight);
    }
    printf("\n");
  }

	print_tour(vhead);
	
  for (vp = vhead; vp != NULL; vp = temp_v) {
    temp_v = vp->next;
    for (adj_v = vp->adj_list; adj_v != NULL; adj_v = temp_adjv) {
      temp_adjv = adj_v->next;
      free(adj_v);
    }
    free(vp);
  }
  return 0;
}
