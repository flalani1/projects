#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "graph.h"

int counter = 0;
/* implement the functions you declare in graph.h here */
/*finished.*/
void add_edge (vertex_t **vtxhead, char *v1_name, char *v2_name, int weight) {
	
	vertex_t *v1, *v2;
	
	v1 = create_vertex(vtxhead,v1_name);
	v2 = create_vertex(vtxhead,v2_name);
	
	addTo_adj_list(&(v1->adj_list),(create_adj_vertex(v2, weight)));
	addTo_adj_list(&(v2->adj_list),(create_adj_vertex(v1,weight)));
}

vertex_t *create_vertex(vertex_t **vtxhead, char *value) {
	
	vertex_t *currentVertex = *vtxhead;
	
	if(*vtxhead == NULL) { //adding our first vertex
		*vtxhead = (vertex_t *) malloc(sizeof(vertex_t));
		currentVertex = *vtxhead;
		counter++;
	}
	
	else {
		
		if(!strcmp((currentVertex->name),value))
		return currentVertex; //this means vertex already exists 
		
		while(1) {
			
			if(!strcmp((currentVertex->name),value))
				return currentVertex;
			if(currentVertex->next == NULL)
				break;
			
			currentVertex = currentVertex->next; //move to the next one
		}
	  
	    //if that fails and we have non empty list
	    currentVertex->next = (vertex_t *) malloc(sizeof(vertex_t));
	    currentVertex= currentVertex->next;
	    counter++;
	}
	
		currentVertex->name = value;
		currentVertex->next = NULL;
		currentVertex->adj_list = NULL;
		currentVertex->visitedVertex = 0;
		
		return currentVertex;
}

adj_vertex_t *create_adj_vertex(vertex_t *vertex_in_list, int weight) {
	//creates a vertex that will go in our adj list
	
	adj_vertex_t *adjVertex = (adj_vertex_t *) malloc(sizeof(adj_vertex_t));
	
	adjVertex->vertex = vertex_in_list;
	adjVertex->edge_weight = weight;
	adjVertex->next = NULL;
	
	return adjVertex;
}

void addTo_adj_list(adj_vertex_t **adjhead, adj_vertex_t *adjv) {
	
	if(*adjhead == NULL)
	*adjhead = adjv;
	
	else {
		adj_vertex_t *adj = *adjhead;
		
		while((adj->next) != NULL) {
			adj = adj->next;
		}
		//now to add the adjv to the list 
		adj->next = adjv;
	}
}

//traverse graph

int find_tour(vertex_t *vtxhead, int count, int cost) {
	
	vertex_t *vtx = NULL;
	adj_vertex_t *adj= NULL;
	int length = -1; 
	
	if(vtxhead != NULL && vtxhead->visitedVertex== 0) {
		
		vtxhead->visitedVertex= 1;
		count++;
		
		if(count == counter) {
			
			printf("%s ",vtxhead->name);
			return cost;
		}
		
		for(adj=vtxhead->adj_list; adj != NULL; adj= adj->next) {
			
			length = find_tour(adj->vertex,count,adj->edge_weight);
			
			if(length > 0) {
				
				printf("%s ",vtxhead->name);
				return (cost + length);
			}
		}
		
			vtxhead->visitedVertex= 0;
			count--;
			
		//no tour found try another vertex
		length = -1;
		
		for(vtx = vtxhead;vtx != NULL && length < 0; vtx= vtx->next) {
			
			length = find_tour(vtxhead->next,count,cost);
		}
		
		return length;
	}
	
		return -1;
}

void print_tour(vertex_t *vtxhead) {
	
	int lengthOfTour = 0;
	printf("\n");
	printf("Tour Path: ");
	lengthOfTour = find_tour(vtxhead,0,0);
	
	if(lengthOfTour > 0) 
		printf("\nTour Length: %d\n", lengthOfTour);
	else 
		printf("No Tour Found\n");
	
}
