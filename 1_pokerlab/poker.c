#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "poker.h"

/* converts a hand (of 5 cards) to a string representation, and stores it in the
 * provided buffer. The buffer is assumed to be large enough.
 */
/*finished.*/
void hand_to_string (hand_t hand, char *handstr) {
    char *p = handstr;
    int i;
    char *val, *suit;
    for (i=0; i<5; i++) {
        if (hand[i].value < 10) {
            *p++ = hand[i].value + '0';
        } else {
            switch(hand[i].value) {
            case 10: *p++ = 'T'; break;
            case 11: *p++ = 'J'; break;
            case 12: *p++ = 'Q'; break;
            case 13: *p++ = 'K'; break;
            case 14: *p++ = 'A'; break;
            }
        }
        switch(hand[i].suit) {
        case DIAMOND: *p++ = 'D'; break;
        case CLUB: *p++ = 'C'; break;
        case HEART: *p++ = 'H'; break;
        case SPADE: *p++ = 'S'; break;
        }
        if (i<=3) *p++ = ' ';
    }
    *p = '\0';
}

/* converts a string representation of a hand into 5 separate card structs. The
 * given array of cards is populated with the card values.
 */
void string_to_hand (const char *handstr, hand_t hand) {
  const char *p = handstr;
  int i;
  //to keep track of the card in the hand
  for (i=0; i<5; i++) {

    //VALUE
    //if value is a number from 2-9
    if (*p <= (9 + '0')) {
      hand[i].value = (*p - '0');
      p++;
    }
    //if value is a number 10-A
    else {
      switch(*p) {
      case 'A': hand[i].value = 14;
	break;
      case 'K': hand[i].value = 13;
	break;
      case 'Q': hand[i].value = 12;
	break;
      case 'J': hand[i].value = 11;
	break;
      case 'T': hand[i].value = 10;
	break;
      }
      p++;
    }

    //SUIT
    //check suit
    switch(*p) {
    case 'D': hand[i].suit = DIAMOND;
      break;
    case 'C': hand[i].suit = CLUB;
      break;
    case 'H': hand[i].suit = HEART;
      break;
    case 'S': hand[i].suit = SPADE;
      break;
    }
    p++;
    if (*p == ' ') {
      p++;
    }
  }
}

/* sorts the hands so that the cards are in ascending order of value (two
 * lowest, ace highest */
void sort_hand (hand_t hand) {
   
  int i, j, lowest, lowspot;
  card_t temp;
  
  for (i=0; i<5; i++) {
    lowest = hand[i].value;
    lowspot = i;
    for (j=i+1; j<5; j++) {
      if (lowest > hand[j].value) {
	lowest = hand[j].value;
	lowspot = j;
      }
    }
    temp = hand[i];
    hand[i] = hand[lowspot];
    hand[lowspot] = temp;
  }
  
}

//checks the number of pairs
//returns 0 for no pairs, 1 for 1 pair, 2 for 2 pairs, 3 for 3ofakind, 4 for 4ofakind, and 5 for fullhouse
int count_pairs (hand_t hand) {
    int i, j;
    int count = 0;
    sort_hand(hand);
    for (i=0; i<5; i++) {
      for (j=i+1; j<5; j++){
	if (hand[i].value == hand[j].value) {
            count ++;
	}
      }
    }
    //only 4 of a kind
    if (count == 6)
      return 4;
    //fullhouse
    if (count == 4)
      return 5;
  return count;
}

//checks if there are a pair of cards that have the same value
int is_onepair (hand_t hand) {
  if (count_pairs(hand) == 1)
    return 1;
  if (is_twopairs(hand))
    return 1;
  if (is_threeofakind(hand))
    return 1;
  if (is_fullhouse(hand))
    return 1;
  if (is_fourofakind(hand))
    return 1;
  return 0;
}

//checks if there are two pairs of cards that have the same value
int is_twopairs (hand_t hand) {
  if (count_pairs(hand) == 2)
    return 1;
  if (is_fourofakind(hand))
    return 1;
  return 0;
}

//checks if there are three cards that have the same value
int is_threeofakind (hand_t hand) {
  if (count_pairs(hand) == 3)
    return 1;
  if (is_fullhouse(hand))
    return 1;
  if (is_fourofakind(hand))
    return 1;
  return 0;
}

//checks if card values are in numeric order
int is_straight (hand_t hand) {
  sort_hand(hand);
  int i;
  for (i=0; i<4; i++) {
    if (((hand[i].value)+1) != (hand[i+1].value)) {
      if (i == 3) {
	if ((hand[3].value == 5) && (hand[4].value == 14))
	  return 1;
      }
      return 0;
    }
  }
  return 1;
}

//checks if there are a set of 3 cards with the same value and a set of 
//two cards with the same value (2 values total)
int is_fullhouse (hand_t hand) {
  if (count_pairs(hand) == 5)
    return 1;
  return 0;
}

//checks if all cards have the same suit
int is_flush (hand_t hand) {
  if ((hand[0].suit == hand[1].suit) && (hand[0].suit ==  hand[2].suit) && (hand[0].suit == hand[3].suit) && (hand[0].suit == hand[4].suit))
    return 1;
  return 0;
}

//checks if all cards have the same suit and are in numeric order
int is_straightflush (hand_t hand) {
  if (is_straight(hand) && is_flush(hand)) 
    return 1;
  return 0;
}

//checks if there are 4 cards that have the same value
int is_fourofakind (hand_t hand) {
  if (count_pairs(hand) == 4)
    return 1;
  return 0;
}

//checks if all cards have the same suit, are in numeric order and are values 10-14
int is_royalflush (hand_t hand) {
  if (is_straight(hand) && is_flush(hand)) {
    if (hand[0].value == 10) {
      return 1;
    }
  }
  return 0;
}

/* compares the hands based on rank -- if the ranks (and rank values) are
 * identical, compares the hands based on their highcards.
 * returns 0 if h1 > h2, 1 if h2 > h1.
 */
int compare_hands (hand_t h1, hand_t h2) {
  int h1_rankvalue;
  int h2_rankvalue;

  //royal flush
  if (is_royalflush(h1) || is_royalflush(h2)) {
    if (is_royalflush(h1))
	return 0;
    else return 1;
  }

  //four of a kind
  else if (is_fourofakind(h1) || is_fourofakind(h2)) {
    if (is_fourofakind(h1) && (!(is_fourofakind(h2))))
      return 0;
    else if (is_fourofakind(h2) && (!(is_fourofakind(h1))))
      return 1;
    else {
      sort_hand(h1);
      sort_hand(h2);
      //hand 1
      if (h1[0].value == h1[1].value)
	h1_rankvalue = h1[0].value;
      else 
	h1_rankvalue = h1[1].value;
      //hand 2
      if (h2[0].value == h2[1].value)
	h2_rankvalue = h2[0].value;
      else 
	h2_rankvalue = h2[1].value;
      //compare rank values
      if (h1_rankvalue >  h2_rankvalue)
	return 0;
      else if (h1_rankvalue < h2_rankvalue) 
	return 1;
      else 
	return compare_highcards (h1, h2);
    }
  }

  //straight flush
  else if (is_straightflush(h1) || is_straightflush(h2)) {
    if (is_straightflush(h1) && (!(is_straightflush(h2))))
      return 0;
    else if (is_straightflush(h2) && (!(is_straightflush(h1))))
      return 1;
    else
      return compare_highcards (h1, h2);
  }

  //flush
  else if (is_flush(h1) || is_flush(h2)) {
    if (is_flush(h1) && (!(is_flush(h2))))
      return 0;
    else if (is_flush(h2) && (!(is_flush(h1))))
      return 1;
    else 
      return compare_highcards(h1, h2);
  }

  //fullhouse
  else if (is_fullhouse(h1) || is_fullhouse(h2)) {
    if (is_fullhouse(h1) && (!(is_fullhouse(h2))))
      return 0;
    else if (is_fullhouse(h2) && (!(is_fullhouse(h1))))
      return 1;
    else
      return compare_highcards(h1, h2);
  }

  //straight
  else if (is_straight(h1) || is_straight(h2)) {
    if (is_straight(h1) && (!(is_straight(h2))))
      return 0;
    else if (is_straight(h2) && (!(is_straight(h1))))
      return 1;
    else 
      return compare_highcards(h1, h2);
  }

  //three of a kind
  else if (is_threeofakind(h1) || is_threeofakind(h2)) {
    if (is_threeofakind(h1) && (!(is_threeofakind(h2))))
      return 0;
    else if (is_threeofakind(h2) && (!(is_threeofakind(h1)))) {
      return 1;
    }
    else {
      sort_hand(h1);
      sort_hand(h2);
      //hand 1
      if (h1[4].value == h1[3].value)
	h1_rankvalue = h1[4].value;
      else if (h1[3].value == h1[2].value)
	h1_rankvalue = h1[3].value;
      else
	h1_rankvalue = h1[2].value;
      //hand 2
      if (h2[4].value ==h2[3].value)
	h2_rankvalue = h2[4].value;
      else if (h2[3].value == h2[2].value)
	h2_rankvalue = h2[3].value;
      else
	h2_rankvalue = h2[2].value;
      //compare rank values
      if (h1_rankvalue > h2_rankvalue) 
	return 0;
      else if (h1_rankvalue < h2_rankvalue)
	return 1;
      //impossible if you play with a normal deck of cards, but I still put it in...
      else
	return compare_highcards(h1, h2);
    }
  }
  
  //two pairs
  else if (is_twopairs(h1) || is_twopairs(h2)) {
    if (is_twopairs(h1) && (!(is_twopairs(h2))))
      return 0;
    else if (is_twopairs(h2)&& (!(is_twopairs(h1))))
      return 1;
    else {
      sort_hand(h1);
      sort_hand(h2);
      //hand 1
      if (h1[4].value == h1[3].value)
	h1_rankvalue = h1[4].value;
      else
	h1_rankvalue = h1[3].value;
      //hand 2
      if (h2[4].value == h2[3].value) 
	h2_rankvalue = h2[4].value;
      else
	h2_rankvalue = h2[3].value;
      //compare rank values
      if (h1_rankvalue > h2_rankvalue)
	return 0;
      else if (h1_rankvalue < h2_rankvalue)
	return 1;
      else 
	return compare_highcards(h1, h2);
    }
  }

  //one pair
  else if (is_onepair(h1) || is_onepair(h2)) {
    if (is_onepair(h1) && (!(is_onepair(h2))))
      return 0;
    else if (is_onepair(h2) && (!(is_onepair(h1))))
      return 1;
    else {
      sort_hand(h1);
      sort_hand(h2); 
      //hand 1
      if (h1[4].value == h1[3].value)
	h1_rankvalue = h1[4].value;
      else if (h1[3].value == h1[2].value)
	h1_rankvalue = h1[3].value;
      else if (h1[2].value == h1[1].value)
	h1_rankvalue = h1[2].value;
      else
	h1_rankvalue = h1[1].value;
     //hand 2
      if (h2[4].value == h2[3].value)
	h2_rankvalue = h2[4].value;
      else if (h2[3].value == h2[2].value)
	h2_rankvalue = h2[3].value;
      else if (h2[2].value == h2[1].value)
	h2_rankvalue = h2[2].value;
      else
	h2_rankvalue = h2[1].value;
      //compare rank values
      if (h1_rankvalue > h2_rankvalue)
	return 0;
      else if (h1_rankvalue < h2_rankvalue)
	return 1;
      else 
	return compare_highcards (h1, h2);
    }
  }

  //no rank
  else 
    return compare_highcards(h1, h2);
}

/* compares the hands based solely on their highcard values (ignoring rank). if
 * the highcards are a draw, compare the next set of highcards, and so forth.
 */
int compare_highcards (hand_t h1, hand_t h2) {
  sort_hand(h1);
  sort_hand(h2);
  int i;
  for (i=4; i>-1; i--) {
    if (h1[i].value > h2[i].value)
      return 0;
    else if (h1[i].value < h2[i].value)
      return 1;
  }
}
