/*Faiza Lalani
 * CS201 Final Project
 * Date Class
 * This class defines the date by the year, month, and day
 */
public class LalaniFaizaDate {

	//instance variables
	int year;
	int month;
	int day;
	
	//default constructor
	public LalaniFaizaDate() {
		year = 1;
		month = 1;
		day = 1;
	}
	
	//non-default constructor
	public LalaniFaizaDate(int y, int m, int d) {
		year = y;
		month = m;
		day = d;
	}
	
	//accessors
	public int getYear() {
		return year;
	}
	
	public int getMonth() {
		return month;
	}
	
	public int getDay() {
		return day;
	}
	
	//mutators
	public void setYear(int y1) {
		year = y1;
	}
	
	public void setMonth(int m1) {
		month = m1;
	}
	
	public void setDay(int d1) {
		day = d1;
	}
	
	//toString
	public String toString() {
		return month + "/" + day + "/" + year;
	}
	
	//equals
	public boolean equals(LalaniFaizaDate d) {
		if ((year == d.getYear()) && (month == d.getMonth()) && (day == d.getDay())) {
			return true;
		}
		return false;
		
	}
}
