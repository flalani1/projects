/*Faiza Lalani
 * CS201 Final Project
 * Doctor Class
 * This class defines a doctor by name and specialty.
 */

public class LalaniFaizaDoctor {
	
	//instance variables
	private String name;
	private String specialty;
	
	
	//default constructor
	public LalaniFaizaDoctor() {
		name = "John Doe";
		specialty = "Gastroenterologist";
	}
	
	//non-default constructor
	public LalaniFaizaDoctor(String n, String s) {
		name = n;
		specialty = s;
	}
	
	//accessors
	public String getName() {
		return name;
	}
	
	public String getSpecialty() {
		return specialty;
	}
	
	//mutators
	public void setName(String n1) {
		name = n1;
	}
	
	public void setSpecialty(String s1) {
		specialty = s1;
	}
	
	//toString
	public String toString() {
		return "This is Dr. " + name + " who specializes in " + specialty;
	}
}
