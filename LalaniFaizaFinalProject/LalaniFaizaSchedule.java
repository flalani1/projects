/*Faiza Lalani
 * CS201 Final Project
 * Schedule Class
 * This class holds an array of appointments and the doctor for these appointments.
 */
public class LalaniFaizaSchedule {

	//instance variables
	LalaniFaizaDoctor doctor;
	LalaniFaizaAppointmentArrayList apptList;
	
	//default constructor
	public LalaniFaizaSchedule() {
		doctor = new LalaniFaizaDoctor();
		apptList = new LalaniFaizaAppointmentArrayList();
	}
	
	//non-default constructor
	public LalaniFaizaSchedule(LalaniFaizaDoctor d, LalaniFaizaAppointmentArrayList aal) {
		doctor = d;
		apptList = aal;
	}
	
	//accessors
	public LalaniFaizaDoctor getDoctor() {
		return doctor;
	}
	
	public LalaniFaizaAppointmentArrayList getApptList() {
		return apptList;
	}
	
	//mutators
	public void setDoctor(LalaniFaizaDoctor d1) {
		doctor = d1;
	}
	
	public void setApptList(LalaniFaizaAppointmentArrayList appl1) {
		apptList = appl1;
	}
	
	//toString
	public String toString() {
		return "This schedule is for " + doctor + " with the appointment list " + apptList.toString();
	}
}
