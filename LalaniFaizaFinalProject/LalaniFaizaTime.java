/*Faiza Lalani
 * CS201 Final Project
 * Time Class
 * This class defines time by the hours and minutes.
 */
public class LalaniFaizaTime {
	
	//instance variables
	private int hours;
	private int minutes;
	
	//default constructor
	public LalaniFaizaTime() {
		hours=0;
		minutes=0;
	}
	
	
	//non-default constructor
	public LalaniFaizaTime(int h, int m) {
		hours=h;
		minutes=m;
	}
	
	//accessors
	public int getHours() {
		return hours;
	}
	
	public int getMinutes() {
		return minutes;
	}
	
	//mutators
	public void setHours(int h1) {
		hours = h1;
	}
	
	public void setMinutes(int m1) {
		minutes = m1;
	}
	
	//toString
	public String toString() {
		if (minutes == 0) 
			return hours + ":00";
		return hours + ":" + minutes;
	}

	//equals
	public boolean equals(LalaniFaizaTime t) {
		if (hours == t.getHours() && minutes == t.getMinutes())
			return true;
		return false;
	}
}
