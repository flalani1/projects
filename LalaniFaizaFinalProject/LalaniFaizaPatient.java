/*Faiza Lalani
 * CS201 Final Project
 * Patient Class
 * This class defines a patient by name, age, and gender
 */
public class LalaniFaizaPatient {

	//instance variables
	String name;
	int age;
	char gender;
	
	//default constructor
	public LalaniFaizaPatient() {
		name = "Jane Doe";
		age = 0;
		gender = 'F';
	}
	
	//non-default constructor
	public LalaniFaizaPatient(String n, int a, char g) {
		name = n;
		age = a;
		gender = g;
	}
	
	//accessors
	public String getName() {
		return name;
	}
	
	public int getAge() {
		return age;
	}
	
	public char getGender() {
		return gender;
	}
	
	//mutators
	public void setName(String n1) {
		name = n1;
	}
	
	public void setAge(int a1) {
		age = a1;
	}
	
	public void setGender(char g1) {
		gender = g1;
	}
	
	//toString
	public String toString() {
		return "The patient is " + name + " with age " + age + " with gender " + gender;
	}
	
	//equals
	public boolean equals(LalaniFaizaPatient p) {
		if (name.equals(p.getName()) && (age == p.getAge()) && (gender == p.getGender())) {
			return true;
		}
		
		return false;
		
	}
}
