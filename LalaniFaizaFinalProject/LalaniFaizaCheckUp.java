/*Faiza Lalani
 * CS201 Final Project
 * CheckUp Appointment Class
 * This class defines a Check Up Appointment based on duration (between 10-15 minutes), weight and height
 * This class has an inheritance relationship with Appointment
 */
public class LalaniFaizaCheckUp extends LalaniFaizaAppointment{
	
	//instance variables
	private double weight;
	private double height;
	
	//default constructor
	public LalaniFaizaCheckUp() {
		super();
		weight = 0.0;
		height = 0.0;
	}
	
	//non-default constructor1
	public LalaniFaizaCheckUp(double w, double h) {
		super();
		weight = w;
		height = h;
	}
	
	//non-default constructor2
	public LalaniFaizaCheckUp(LalaniFaizaPatient p, LalaniFaizaDate d, LalaniFaizaTime t, double w, double h) {
		super(p,d,t);
		weight = w;
		height = h;
	}
	
	//accessors
	public int getDuration() {
		return 10;
	}
	
	public double getHeight() {
		return height;
	}
	
	public double getWeight() {
		return weight;
	}
	
	//mutators
	public void setHeight(double h1) {
		height = h1;
	}
	
	public void setWeight(double w1) {
		weight = w1;
	}
	
	//toString
	public String toString() {
		return "Appointment Type: CheckUp, Date: " + super.getDate() + ", Time: " +  super.getTime() + ", Patient: " + super.getPatient() + ", Duration: " + 10 + ", Patient Height: " + height + ", Patient Weight: " + weight;
	}
	
	//equals
	
	
	
	
	
	
	
	
	
	
	

}
