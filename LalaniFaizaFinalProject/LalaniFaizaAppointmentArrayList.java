/*Faiza Lalani
 * CS201 Final Project
 * Appointment ArrayList Class
 * This is an ArrayList class that holds Appointment objects (included inherited Appointment objects)
 */
public class LalaniFaizaAppointmentArrayList {

	//instance variables
	final int MAX_SIZE = 210; //7 hours, 6 appointments/hour (if all are 10min check ups), 5 days/week --> 7*6*5 = 210
	private LalaniFaizaAppointment[] apptArray;
	private int index;
	private int iterator;
	
	//default constructor
	public LalaniFaizaAppointmentArrayList() {
		apptArray = new LalaniFaizaAppointment [MAX_SIZE];
		index = 0;
		iterator = 0;
	}
	
	//non-default constructor
	public LalaniFaizaAppointmentArrayList(int size) {
		if (size<MAX_SIZE && size>0) 
			apptArray = new LalaniFaizaAppointment[size];
		else {
			System.out.println("This cannot be the number of appointments in a week. The number of appointments will be set to the maximum of 210.");
			apptArray = new LalaniFaizaAppointment[MAX_SIZE];
		}
		index = 0;
		iterator = 0;
	}
	
	//accessors
	public int getMax() {
		return MAX_SIZE;
	}

	public int getIndex() {
		return index;
	}
	
	public LalaniFaizaAppointment[] getApptArray() {//(returns a copy of the array)
		LalaniFaizaAppointment[] temp = new LalaniFaizaAppointment[apptArray.length];
		for (int i=0; i<apptArray.length;i++) {
			temp[i] = apptArray[i];
		}
		return temp;
	}
	
	public int getLength() {
		return apptArray.length;
	}
	
	//mutator
	public void setApptArray(LalaniFaizaAppointment[] a) {
		for (int i=0; i<apptArray.length;i++) {
			apptArray[i] = a[i];
		}
	}
	
	//toString
	public String toString() {
		System.out.println("The appointments on the schedule are: ");
		for(int i=0; i<index; i++) {
			System.out.println((i+1) + ". " + apptArray[i].toString());
		}
		return "";	
	}
	
//Array Manipulations
	//add more capacity to the array
	public void moreCapacity() {
		LalaniFaizaAppointment[] temporary = new LalaniFaizaAppointment[MAX_SIZE+10];
		for(int i=0;i<apptArray.length;i++) {
			temporary[i]=apptArray[i];
		}
		apptArray = temporary;
	}

	//remove excess spaces in array
	public void trim() {
		LalaniFaizaAppointment[] temp = new LalaniFaizaAppointment[index];
		for (int i = 0; i < index; i++) {
			temp[i] = apptArray[i];
		}
		apptArray = temp;
	}

//Sorts
	//sorts by year
	public void sortYear() {
		for (int i=0;i<index;i++) {
			for(int j=i+1;j<index;j++) {
				if (apptArray[i].getDate().getYear() > apptArray[j].getDate().getYear()) {
					swapElts(apptArray[i], apptArray[j]);
				}
			}
		}
	}
	
	//sorts by month, if the year is the same
	public void sortMonth() {
		for (int i=0;i<index;i++) {
			for(int j=i+1;j<index;j++) {
				if (apptArray[i].getDate().getYear() == apptArray[j].getDate().getYear()) {
					if (apptArray[i].getDate().getMonth() > apptArray[j].getDate().getMonth()) {
						swapElts(apptArray[i], apptArray[j]);
				
					}
				}
			}
		}
	}
	
	//sorts by day if year and month are the same
	public void sortDay() {
		for (int i=0;i<index;i++) {
			for(int j=i+1;j<index;j++) {
				if ((apptArray[i].getDate().getYear() == apptArray[j].getDate().getYear()) && (apptArray[i].getDate().getMonth() == apptArray[j].getDate().getMonth())) {
					if (apptArray[i].getDate().getDay() > apptArray[j].getDate().getDay()) {
						swapElts(apptArray[i], apptArray[j]);
					}
				}
			}
		}
	}
	
	//sorts by hours if dates are the same
	public void sortHours() {
		for (int i=0;i<index;i++) {
			for(int j=i+1;j<index;j++) {
				if ((apptArray[i].getDate().getYear() == apptArray[j].getDate().getYear()) && (apptArray[i].getDate().getMonth() == apptArray[j].getDate().getMonth()) && (apptArray[i].getDate().getDay() == apptArray[j].getDate().getDay())) {
					if (apptArray[i].getTime().getHours() > apptArray[j].getTime().getHours()) {
						swapElts(apptArray[i], apptArray[j]);
					}
				}
			}
		}
	}	
	
	//sorts by minutes if 
	public void sortMinutes() {
		for (int i=0;i<index;i++) {
			for(int j=i+1;j<index;j++) {
				if ((apptArray[i].getDate().getYear() == apptArray[j].getDate().getYear()) && (apptArray[i].getDate().getMonth() == apptArray[j].getDate().getMonth()) && (apptArray[i].getDate().getDay() == apptArray[j].getDate().getDay()) && (apptArray[i].getTime().getHours() == apptArray[j].getTime().getHours())) {
					if (apptArray[i].getTime().getMinutes() > apptArray[j].getTime().getMinutes()) {
						swapElts(apptArray[i], apptArray[j]);
					}
				}
			}
		}
	}	
	
	//sort with all categories
	public void sortArray() {
		sortYear();
		sortMonth();
		sortDay();
		sortHours();
		sortMinutes();
		//printout
			for(int n=0;n<index;n++) 
				System.out.println(apptArray[n]);
	}
	
	//checks if the appointment array is full
	public boolean isFull() {
		if (index == (apptArray.length-1))
			return true;
		return false;
	}
	
	//checks if the appointment array is empty
	public boolean isEmpty() {
		if (index == 0) 
			return true;
		return false;
	}
	
//Appointment manipulations
	//return the appointment at the given position
	public LalaniFaizaAppointment getElt(int pos) {
		return apptArray[pos];
	}
	
	//find an appointment by the patient name and return its position
	public int findElt(String name) {
		for (int i=0;i<index;i++) {
			if (((apptArray[i].getPatient()).getName()).equals(name)) {
				return i;
			}
		}
		return -1;
	}
	
	//find an appointment by the date and return its position
	public int findElt(LalaniFaizaDate d) {
		int found = -1;
		for (int i=0;i<index;i++) {
			if ((apptArray[i].getDate()).equals(d)) {
				System.out.println(i + ". " + apptArray[i].toString());
				found = i;
			}
		}
		return found;
	}
	
	//adds an appointment to the end of the appointment array
	public boolean addElt(LalaniFaizaAppointment a0) {
		if (!isFull()) {
			for(int i=0;i<index;i++) {
				if (apptArray[i].sameDate(a0) && apptArray[i].sameTime(a0)) {
					return false;
				}
			}
			apptArray[index]=a0;
			index++;
			return true;
		}
		return false;
	}
	
	//removes an appointment from the appointment array
	public boolean removeElt(LalaniFaizaAppointment a9) {
		int temp=-1;
		for (int i=0;i<index;i++) {
			if (apptArray[i].equals(a9))
				temp = i;
		}//find position of a9
				
		if (temp != -1) { //if a9 is found in the array
			for(int j=temp;j<index;j++) {//move all appointments up one slot
				apptArray[j] = apptArray[j+1];
			}
			index--; //set index to current open position
			return true; //element has been removed
		}
		
		//if appointment was not found in the array
		return false;
	}
	
	//swaps two appointments with each other. Used for sorting.
	public void swapElts(LalaniFaizaAppointment a1, LalaniFaizaAppointment a2) {
		int pos1 = -1;
		int pos2 = -1;
		for (int i=0; i<index;i++) {
			if (apptArray[i].equals(a1))
				pos1 = i;
			if (apptArray[i].equals(a2)) 
				pos2 = i;
		}
		if ((pos1 != pos2) && (pos1 != -1) && (pos2 != -1)) {
			apptArray[pos2]=a1;
			apptArray[pos1]=a2;
		}
	}
	
//Iterator manipulations
	//sets iterator to zero
	public void reset() {
		iterator = 0;
	}
	
	//checks if there are any more appointments in the appointment array
	public boolean hasNext() {
		if (apptArray[iterator+1] != null) 
			return true;
		return false;
	}
	
	//returns the next appointment in the array
	public LalaniFaizaAppointment getNext() {
		iterator++;
		if (iterator == apptArray.length) {
			reset();
			return apptArray[apptArray.length-1];
		}
		return apptArray[iterator-1];
	}
	
}

	


