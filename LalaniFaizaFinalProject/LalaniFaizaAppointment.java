/*Faiza Lalani
 * CS201 Final Project
 * Appointment Class
 * This class defines an appointment with a patient, date, and time
 */
public class LalaniFaizaAppointment {
	
	//instance variables
	private LalaniFaizaPatient patient;
	private LalaniFaizaDate date;
	private LalaniFaizaTime time;
	
	
	//default constructor
	public LalaniFaizaAppointment() {
		patient = new LalaniFaizaPatient();
		date = new LalaniFaizaDate();
		time = new LalaniFaizaTime();
		
	}
	
	//non-default constructor 2
	public LalaniFaizaAppointment(LalaniFaizaPatient p, LalaniFaizaDate d, LalaniFaizaTime t) {
		patient = p;
		date = d;
		time = t;
	}
	
	//accessors
	public LalaniFaizaPatient getPatient() {
		return patient;
	}
	
	public LalaniFaizaDate getDate() {
		return date;
	}
	
	public LalaniFaizaTime getTime() {
		return time;
	}
	
	//mutators
	public void setPatient(LalaniFaizaPatient p1) {
		patient = p1;
	}
	
	public void setDate(LalaniFaizaDate d1) {
		date = d1;
	}
	
	public void setTime(LalaniFaizaTime t1) {
		time = t1;
	}
	
	//toString()
	public String toString() {
		return "Date: " + date + ", Time: " + time.toString() + ", Patient: " + patient;
	}
	
	//checks if the date of two appointments is the same
	public boolean sameDate(LalaniFaizaAppointment a) {
		if(date.getYear() == a.getDate().getYear()) {
			if (date.getMonth() == a.getDate().getMonth()) {
				if (date.getDay() == a.getDate().getDay()) {
					return true;
				}
			}
		}
		return false;
	}
	
	//checks if two times are the same
	public boolean sameTime(LalaniFaizaAppointment a1) {
		if (time.getHours() == a1.getTime().getHours()) {
			if(time.getMinutes() == a1.getTime().getMinutes()) {
				return true;
			}
		}
		return false;
	}
	
	//checks if two patients are the same
	public boolean samePatient(LalaniFaizaAppointment a2) {
		if (patient.getName().equals(a2.getPatient().getName())) {
			if (patient.getAge() == a2.getPatient().getAge()) {
				if (patient.getGender() == a2.getPatient().getGender()) {
					return true;
				}
			}
		}
		return false;
	}
	
	//equals
	public boolean equals(LalaniFaizaAppointment a3) {
		if (this.sameDate(a3) && this.sameTime(a3) && this.samePatient(a3)) {
			return true;
		}
		return false;
	}
	
	
	
	
	
	
	
	
	
	
}
