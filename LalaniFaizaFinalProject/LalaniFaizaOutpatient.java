/*Faiza Lalani
 * CS201 Final Project
 * Outpatient Appointment Class
 * This class defines an Outpatient Appointment based on duration (between 20-30 minutes) and procedure
 * This class has an inheritance relationship with Appointment
 */
public class LalaniFaizaOutpatient extends LalaniFaizaAppointment{
	
	//instance variables
	private int duration;
	private String procedure;
	
	//default constructor
	public LalaniFaizaOutpatient() {
		super();
		duration = 20;
		procedure = "Colonoscopy";
	}
	
	//non-default constructor1
	public LalaniFaizaOutpatient(int d, String p) {
		super();
		if (d>=20 && d<=30)
			duration = d;
		else {
			System.out.println("This duration is not valid for a outpatient procedure. The duration will be set to 20.");
			duration = 20;
		}
		procedure = p;
	}
	
	//non-default constructor2
	public LalaniFaizaOutpatient(LalaniFaizaPatient p1, LalaniFaizaDate d1, LalaniFaizaTime t1, int d, String p) {
		super(p1,d1,t1);
		if (d>=20 && d<=30)
			duration = d;
		else {
			System.out.println("This duration is not valid for a outpatient procedure. The duration will be set to 20.");
			duration = 20;
		}
		procedure = p;
	}
	
	//accessors
	public int getDuration() {
		return duration;
	}
	
	public String getProcedure() {
		return procedure;
	}
	
	//mutators
	public void setDuration(int d1) {
		if (d1>=10 && d1<=15)
			duration = d1;
		else {
			System.out.println("This duration is not valid for a check up. The duration will be set to 10.)");
			duration = 10;
		}
	}
	
	public void setHeight(String p1) {
		procedure = p1;
	}
	
	//toString
	public String toString() {
		return "Appointment Type: Outpatient, Date: " + super.getDate() + ", Time: " +  super.getTime() + ", Patient: " + super.getPatient() + ", Duration: " + duration + ", Procedure: " + procedure;
	}
}
