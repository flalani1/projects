/*Faiza Lalani
 * CS201 Final Project
 * Menu Class
 * This class holds all the options of the menu
 */

import java.io.*;
import java.util.*;

public class LalaniFaizaMenu {

	//instance variables
	File inputFile;
	Scanner inFile;
	LalaniFaizaSchedule schedule;
	Scanner in = new Scanner(System.in);
	BufferedWriter out;

	
	public LalaniFaizaMenu(String f, int n) {
		inputFile = new File(f);
		try {
			inFile = new Scanner(inputFile);
		} catch (FileNotFoundException e) {
			System.out.println("Error with input file");
		}
		schedule = new LalaniFaizaSchedule();
		try {
			out = new BufferedWriter(new FileWriter("LalaniFaizaOutputFile.txt"));
		} catch (IOException e) {
			System.out.print("Error with output file");
		}
	}
	
	//reads files and returns the completed schedule made from the info in the file
	public LalaniFaizaSchedule readFile(int n) {
		String line = "";
		String[] temp = new String[10];
		
		for (int i=0;i<n;i++) {
			line = inFile.nextLine();
			temp = line.split(",");
			LalaniFaizaPatient p1 = new LalaniFaizaPatient(temp[0],Integer.parseInt(temp[1]),temp[2].charAt(0));
			LalaniFaizaDate d1 = new LalaniFaizaDate(Integer.parseInt(temp[3]),Integer.parseInt(temp[4]),Integer.parseInt(temp[5]));
			LalaniFaizaTime t1 = new LalaniFaizaTime(Integer.parseInt(temp[6]),Integer.parseInt(temp[7]));
			if (Double.parseDouble(temp[8])>30) {
				LalaniFaizaCheckUp c1 = new LalaniFaizaCheckUp(p1,d1,t1,Double.parseDouble(temp[8]),Double.parseDouble(temp[9]));
				schedule.apptList.addElt(c1);
			}
			else { 
				LalaniFaizaOutpatient op1 = new LalaniFaizaOutpatient(p1,d1,t1,Integer.parseInt(temp[8]),temp[9]);
				schedule.getApptList().addElt(op1);
			}
		}
		return schedule;
	}
	
	//user enters choice
	public int getChoice() {
		System.out.println("Enter your choice: ");
		return in.nextInt();
	}
	
	//displays choices
	public int displayMenu() {
	//menu options include 
		//add appointment
		//remove appointment
		//sort appointments
		//find appointment
			//find by date
			//find by patient name
			//find by procedure
		//see next appointment
		//see all appointments
		//quit
	System.out.println("This is the patient-doctor scheduler for Dr." + schedule.doctor.getName() + ", who is a " + schedule.doctor.getSpecialty() + ".");
	System.out.println("What would you like to do?");
	System.out.println("1 - Add an appointment to the schedule");
	System.out.println("2 - Remove an appointment from the schedule");
	System.out.println("3 - Sort the appointments on the schedule by the date and time");
	System.out.println("4 - Find an appointment on the schedule");
	System.out.println("5 - See the next appointment on the schedule");
	System.out.println("6 - See all appointments on the schedule");
	System.out.println("0 - Quit the scheduler");
	int choice = getChoice();
		switch (choice) {
			case 1: add();
					displayMenu();
				break;
			case 2: remove();
					displayMenu();
				break;
			case 3: sort();
					displayMenu();
				break;
			case 4: findOptions();
					displayMenu();
				break;
			case 5: System.out.println(next());
					displayMenu();
				break;
			case 6: display();
					displayMenu();
				break;
			case 0:
				break;
			default: System.out.println("This is an invalid entry. Please try again.");
					displayMenu();
				break;
		}
		return -1;
	}
	
	//displays all appointments in the schedule
	public void display() {
		schedule.getApptList().toString();
	}
	
	//add an appointment
	public void add() {
		System.out.println("Please enter information as prompted: "); 
		System.out.println("Patient Name: ");
		String name = in.next();
		System.out.println("Patient Age: ");
		int age = in.nextInt();
		System.out.println("Patient Gender (M or F): ");
		char gender = (in.next().toUpperCase()).charAt(0);
		System.out.println("Please enter the date in numbers. Type the year and click enter, type the month and click enter, then type the day and click enter.");
		int year = in.nextInt();
		int month = in.nextInt();
		int day = in.nextInt();
		System.out.println("Please enter the time 24 hour format. Type the hour and click enter, then type the minutes and click enter.");
		System.out.println("For example, if you would like a 4:30pm appointment, type '16' and click enter, then type '30' and click enter.");
		int hours = in.nextInt();
		int minutes = in.nextInt();
		System.out.println("Would you like to schedule a Check up (C) or Outpatient procedure (O)?");
		char type = (in.next().toUpperCase()).charAt(0);
		if (type == 'C') {
			System.out.println("Please enter patient height in inches: ");
			double height = in.nextDouble();
			System.out.println("Please enter patient weight in pounds: ");
			double weight = in.nextDouble();
			LalaniFaizaPatient p = new LalaniFaizaPatient(name,age,gender);
			LalaniFaizaDate d = new LalaniFaizaDate(year,month,day);
			LalaniFaizaTime t = new LalaniFaizaTime(hours,minutes);
			LalaniFaizaCheckUp c = new LalaniFaizaCheckUp(p,d,t,height,weight);
			(schedule.getApptList()).addElt(c);
			System.out.println("Your appointment has been added!");
			System.out.println("Would you like to see the date, time and duration of your appointment?");
			char choice = ((in.next()).toUpperCase()).charAt(0);
			if (choice == 'Y') {
				System.out.println(c.getDate() + ", " + c.getTime() + ", Duration: " + c.getDuration());
			}
			try {
				out.write("Add test          ");
				out.write(c.toString());
				out.newLine();
			} catch (IOException e) {
				System.out.println("Error writing to output file");
			}
		}
		else if (type == 'O') {
			System.out.println("Please enter procedure duration in minutes (20-30): ");
			int duration = in.nextInt();
			if(duration<20 || duration>30) {
				System.out.println("Incorrect Duration was entered. Duration will be set to 20.");
				duration = 20; 
			}
			System.out.println("Please enter name of procedure: ");
			String procedure = in.next();
			LalaniFaizaPatient p = new LalaniFaizaPatient(name,age,gender);
			LalaniFaizaDate d = new LalaniFaizaDate(year,month,day);
			LalaniFaizaTime t = new LalaniFaizaTime(hours,minutes);
			LalaniFaizaOutpatient o = new LalaniFaizaOutpatient(p,d,t,duration,procedure);
			(schedule.getApptList()).addElt(o);
			System.out.println("Your appointment has been added!");
			System.out.println("Would you like to see the date, time and duration of your appointment?");
			char choice = ((in.next()).toUpperCase()).charAt(0);
			if (choice == 'Y') {
				System.out.println(o.getDate() + ", " + o.getTime() + ", Duration: " + o.getDuration());
			}
			try {
				out.write("Add test          ");
				out.write(o.toString());
				out.newLine();
			} catch (IOException e) {
				System.out.println("Error writing to output file");
			}
		}
		else System.out.println("They appointment type was invalid. Please start from the beginning.");
	}
	
	//remove an appointment
	public void remove() {
		System.out.println("To remove an appointment, we first need to find it.");
		int found = findOptions();
		if (found != -1) {
			schedule.apptList.removeElt(schedule.apptList.getElt(found));
			System.out.println("Your appointment has been removed!");
			try {
				out.write("Remove Test          ");
				out.write(schedule.apptList.getElt(found).toString());
				out.newLine();
			} catch (IOException e) {
				System.out.println("Error writing to output file");
			}
		}
		
	}
	
	//sort appointmentlist
	public void sort() {
		schedule.apptList.sortArray();
		System.out.println("The appointment list has been sorted!");
	}
	
	//options for finding an element
	public int findOptions() {
		System.out.println("Please enter the number next to the option that you would like to use to find your appointment:");
		System.out.println("1 - Find by patient name");
		System.out.println("2 - Find by date");
		System.out.println("3 - Cancel Find");
		int select = in.nextInt();
		switch(select) {
			case 1: 
				return findName();
			case 2:
				return findDate();
			case 3:
				return -1;
			default: System.out.println("Invalid Entry. Please try again.");
			    return findOptions();
		}
		
		
	}
	
	//find an element by name
	public int findName() {
		System.out.println("Please enter the name of the patient: ");
		String name = in.next();
		int found = schedule.apptList.findElt(name);
		if (found != -1) {
			System.out.println("Your appointment has been found!");
			System.out.println(schedule.apptList.getElt(found));
			try {
				out.write("Find Name          ");
				out.write(schedule.apptList.getElt(found).toString());
				out.newLine();
			} catch (IOException e) {
				System.out.println("Error writing to output file");
			}
			return found;
		}
		System.out.println("This patient name was not found.");
		return -1;
	}
	

	//find an element by date
	public int findDate() {
		System.out.println("Please enter the year of the appointment");
		int year = in.nextInt();
		System.out.println("Please enter the month of the appointment");
		int month = in.nextInt();
		System.out.println("Please enter the day of the appointment");
		int day = in.nextInt();
		LalaniFaizaDate d1 = new LalaniFaizaDate(year,month,day);
		schedule.apptList.findElt(d1);
		System.out.println("These are the appointments on the date entered. If you do not see your appointment, it was not found.");
		System.out.println("Please enter the number next to your appointment. If it is not there, please enter -1.");
		int found = in.nextInt();
		if (found != -1) {
			try {
				out.write("Find Date          ");
				out.write(schedule.apptList.getElt(found).toString());
				out.newLine();
			} catch (IOException e) {
				System.out.println("Error writing to output file");
			}
			return found;
		}
		System.out.println("This date was not found.");
		return -1;
	}
	
	//shows next appointment (using iterator)
	public LalaniFaizaAppointment next() {
		return schedule.apptList.getNext();
	}
	
	//write to output file after exit
	public void finalOutput() {
		try {
			out.write("This will show polymorphism.");
			out.newLine();
			System.out.println("The duration for the check up appointment for the default checkup appointment will be written to the output file");
			System.out.println("The duration for the outpatient appointment for the default outpatient appointment will be written to the output file");
			LalaniFaizaCheckUp c = new LalaniFaizaCheckUp();
			LalaniFaizaOutpatient o = new LalaniFaizaOutpatient();
			out.write("CheckUp Duration: " + c.getDuration());
			out.newLine();
			out.write("Outpatient Duration: " + o.getDuration());
			out.newLine();
			System.out.println("Please write a word you would like to see at the end of the output file");
			out.write(in.next());
			out.close();
		} catch (IOException e) {
			System.out.println("Error: Could not write file");
		}
	}
	
}
