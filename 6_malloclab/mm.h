#include <stdio.h>

typedef struct block block;
struct block {
  size_t size;
  block* next;
};

extern int mm_init (void);
extern void *mm_malloc (size_t size);
extern void mm_free (void *ptr);
extern void *mm_realloc(void *ptr, size_t size);

int fit_list(size_t size);
void put_free_block(block *b);
void mark_used(block *b);
block *split(block *b, size_t offset);
block *find_fit(size_t size);
block *create_free_block(size_t size);
void realloc_join(block *b);
int found_free(block *b);

